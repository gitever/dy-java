package com.dyj.applet.domain;

import java.util.List;

/**
 * @author danmo
 * @date 2024-04-28 14:48
 **/
public class SupplierServiceProvider {

    /**
     * 服务商营业执照
     */
    private String business_license_ext_id;

    /**
     * 服务商行业许可证
     */
    private List<String> industry_license_ext_id;

    public static SupplierServiceProviderBuilder builder() {
        return new SupplierServiceProviderBuilder();
    }

    public static class SupplierServiceProviderBuilder {
        private String businessLicenseExtId;
        private List<String> industryLicenseExtId;

        public SupplierServiceProviderBuilder businessLicenseExtId(String businessLicenseExtId) {
            this.businessLicenseExtId = businessLicenseExtId;
            return this;
        }
        public SupplierServiceProviderBuilder industryLicenseExtId(List<String> industryLicenseExtId) {
            this.industryLicenseExtId = industryLicenseExtId;
            return this;
        }
        public SupplierServiceProvider build() {
            SupplierServiceProvider supplierServiceProvider = new SupplierServiceProvider();
            supplierServiceProvider.setBusiness_license_ext_id(businessLicenseExtId);
            supplierServiceProvider.setIndustry_license_ext_id(industryLicenseExtId);
            return supplierServiceProvider;
        }
    }

    public String getBusiness_license_ext_id() {
        return business_license_ext_id;
    }

    public void setBusiness_license_ext_id(String business_license_ext_id) {
        this.business_license_ext_id = business_license_ext_id;
    }

    public List<String> getIndustry_license_ext_id() {
        return industry_license_ext_id;
    }

    public void setIndustry_license_ext_id(List<String> industry_license_ext_id) {
        this.industry_license_ext_id = industry_license_ext_id;
    }
}
