package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

/**
 * @author danmo
 * @date 2024-04-28 10:43
 **/
public class RegisterMaAppQuery extends BaseQuery {

    /**
     * 小程序ID
     */
    private String app_id;

    /**
     * 定制商品
     * 1-定制商品
     * 2-会员营销
     * 3-加购导购
     * 4-权益互动
     * 5-购后课程
     */
    private Integer ecom_microapp_type;

    private String callback_url;

    private String extra;

    private String preview_photo_url;

    public static RegisterMaAppQueryBuilder builder() {
        return new RegisterMaAppQueryBuilder();
    }

    public static class RegisterMaAppQueryBuilder {
        private String appId;
        private Integer ecomMicroappType;
        private String callbackUrl;
        private String extra;
        private String previewPhotoUrl;
        private Integer tenantId;
        private String clientKey;
        public RegisterMaAppQueryBuilder appId(String appId) {
            this.appId = appId;
            return this;
        }
        public RegisterMaAppQueryBuilder ecomMicroappType(Integer ecomMicroappType) {
            this.ecomMicroappType = ecomMicroappType;
            return this;
        }
        public RegisterMaAppQueryBuilder callbackUrl(String callbackUrl) {
            this.callbackUrl = callbackUrl;
            return this;
        }
        public RegisterMaAppQueryBuilder extra(String extra) {
            this.extra = extra;
            return this;
        }
        public RegisterMaAppQueryBuilder previewPhotoUrl(String previewPhotoUrl) {
            this.previewPhotoUrl = previewPhotoUrl;
            return this;
        }
        public RegisterMaAppQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }
        public RegisterMaAppQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }
        public RegisterMaAppQuery build() {
            RegisterMaAppQuery registerMaAppQuery = new RegisterMaAppQuery();
            registerMaAppQuery.setApp_id(appId);
            registerMaAppQuery.setEcom_microapp_type(ecomMicroappType);
            registerMaAppQuery.setCallback_url(callbackUrl);
            registerMaAppQuery.setExtra(extra);
            registerMaAppQuery.setPreview_photo_url(previewPhotoUrl);
            registerMaAppQuery.setTenantId(tenantId);
            registerMaAppQuery.setClientKey(clientKey);
            return registerMaAppQuery;
        }
    }

    public String getApp_id() {
        return app_id;
    }

    public void setApp_id(String app_id) {
        this.app_id = app_id;
    }

    public Integer getEcom_microapp_type() {
        return ecom_microapp_type;
    }

    public void setEcom_microapp_type(Integer ecom_microapp_type) {
        this.ecom_microapp_type = ecom_microapp_type;
    }

    public String getCallback_url() {
        return callback_url;
    }

    public void setCallback_url(String callback_url) {
        this.callback_url = callback_url;
    }

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }

    public String getPreview_photo_url() {
        return preview_photo_url;
    }

    public void setPreview_photo_url(String preview_photo_url) {
        this.preview_photo_url = preview_photo_url;
    }
}
