package com.dyj.applet.domain;

/**
 * @author danmo
 * @date 2024-04-28 16:12
 **/
public class SupplierMatchStatus {

    /**
     * 匹配状态，0-没有匹配，1-匹配中，2-匹配完成，3-匹配失败
     */
    private Integer status;
    /**
     * 商户ID
     */
    private Integer supplier_ext_id;
    /**
     * 匹配任务ID
     */
    private Integer task_id;
    /**
     * 抖音POIID
     */
    private Integer poi_id;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getSupplier_ext_id() {
        return supplier_ext_id;
    }

    public void setSupplier_ext_id(Integer supplier_ext_id) {
        this.supplier_ext_id = supplier_ext_id;
    }

    public Integer getTask_id() {
        return task_id;
    }

    public void setTask_id(Integer task_id) {
        this.task_id = task_id;
    }

    public Integer getPoi_id() {
        return poi_id;
    }

    public void setPoi_id(Integer poi_id) {
        this.poi_id = poi_id;
    }
}
